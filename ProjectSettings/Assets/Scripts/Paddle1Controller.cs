﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paddle1Controller : MonoBehaviour {

    public float paddleSpeed = 1;
    public Vector3 playerPos;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        float yPos = gameObject.transform.position.y + (Input.GetAxis("Vertical")* paddleSpeed);
        playerPos = new Vector3(-9, Mathf.Clamp(yPos, -5.5f, 2.8f),0);
        gameObject.transform.position = playerPos;
	}
}
