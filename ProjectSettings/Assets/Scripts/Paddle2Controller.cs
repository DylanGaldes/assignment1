﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paddle2Controller : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 mousePos = Input.mousePosition;
        Vector3 paddlePos= Camera.main.ScreenToWorldPoint(mousePos);
        transform.position = new Vector3(12.2f, Mathf.Clamp(paddlePos.y, -5.5f, 2.8f));
    }
}
