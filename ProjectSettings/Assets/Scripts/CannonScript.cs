﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonScript : MonoBehaviour {

    private float xDir, yDir;

	// Use this for initialization
	void Start () {
        xDir = Random.Range(-1f, 1f);
        yDir = Random.Range(-1f, 1f);
	}
	
	// Update is called once per frame
	void Update () {
        //rotate the asteroid on its own
        transform.Rotate(Vector3.forward * 10 * Time.deltaTime);
        //Asteroids move, but if they leave screen its bye
        transform.Translate(new Vector3(xDir, yDir) * 10 * Time.deltaTime);
    }
}
