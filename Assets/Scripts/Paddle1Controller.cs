﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paddle1Controller : MonoBehaviour {
    //Paddle speed
    public float paddleSpeed = 1;
    //Player position
    public Vector3 playerPos;
	
	// Update is called once per frame
	void FixedUpdate () {
        float yPos = gameObject.transform.position.y + (Input.GetAxis("Vertical")* paddleSpeed);
        playerPos = new Vector3(-8, Mathf.Clamp(yPos, -3.5f, 3.5f),0);
        gameObject.transform.position = playerPos;
	}
}
