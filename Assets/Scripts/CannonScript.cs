﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CannonScript : MonoBehaviour {

    //Initial Velocity
    public static float velocity = 0f;

	void Start () {
        velocity = checkLevelForVelocity();
        Random rnd = new Random();
        int num = Random.Range(1, 3);
        //First Direction is random
        if (num == 1)
        {
            GetComponent<Rigidbody2D>().velocity = Vector2.right * velocity;
        }
        else if (num == 2)
        {
            GetComponent<Rigidbody2D>().velocity = Vector2.left * velocity;
        }
	}
    void OnCollisionEnter2D(Collision2D col)
    {
        

        // if ball hits the pirate paddle
        if (col.gameObject.name == "PiratePaddle")
        {
            // Calculate hit Factor
            float hit = hitFactor(transform.position,
                                col.transform.position,
                                col.collider.bounds.size.y);

            // Calculate direction, make length=1 via .normalized
            Vector2 dir = new Vector2(1, hit).normalized;

            // Set Velocity
            GetComponent<Rigidbody2D>().velocity = dir * velocity;
        }

        // ball hits British army pedal
        if (col.gameObject.name == "royalArmyPedal")
        {
            // Calculate hit Factor
            float hit = hitFactor(transform.position,
                                col.transform.position,
                                col.collider.bounds.size.y);

            // Calculate direction, make length=1 via .normalized
            Vector2 dir = new Vector2(-1, hit).normalized;

            // Set Velocity 
            GetComponent<Rigidbody2D>().velocity = dir * velocity;
        }
    }

    //Calculates which part of the racket the ball hit
    float hitFactor(Vector2 ballPos, Vector2 racketPos,
                float racketHeight)
    {
        return (ballPos.y - racketPos.y) / racketHeight;
    }
    //Sets velocity depending on which level/ scene you are
    public float checkLevelForVelocity()
    {
        //Stores the name of the current Scene
        string currentScene = SceneManager.GetActiveScene().name.ToString();

        float velocity = 0;
        if (currentScene == "Level1")
        {
            velocity = 20f;
        }
        else if (currentScene == "Level2")
        {
            velocity = 21f;
        }
        else if (currentScene == "Level3")
        {
            velocity = 22f;
        }
        return velocity;
    }
}
