﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstaclesScript : MonoBehaviour {

    private float speed;

    // Use this for initialization
    void Start () {
        //paddle speed
        speed = 5f;
    }
	
	// Update is called once per frame
	void Update () {
        //Moving the obstales
        transform.Translate(new Vector3(0, 1) * speed * Time.deltaTime, Space.World);
        //Keep the obsatcles
        checkMargins(transform);

    }
    //checking the margins of the screen
    public void checkMargins(Transform transform)
    {
        
        if (transform.position.y > Camera.main.orthographicSize)
        {
            transform.position = new Vector3(transform.position.x, -Camera.main.orthographicSize + 0.5f);
        }

        if (transform.position.y < -Camera.main.orthographicSize)
        {
            transform.position = new Vector3(transform.position.x, Camera.main.orthographicSize - 0.5f);
        }
    }
}
