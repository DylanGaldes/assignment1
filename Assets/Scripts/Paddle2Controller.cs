﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paddle2Controller : MonoBehaviour {

	
	// Update is called once per frame
	void FixedUpdate () {
        //Gets mouse position
        Vector3 mousePos = Input.mousePosition;
        //sets paddle Position to mouse position
        Vector3 paddlePos= Camera.main.ScreenToWorldPoint(mousePos);

        transform.position = new Vector3(8f, Mathf.Clamp(paddlePos.y, -3.5f, 3.5f));
    }
}
