﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Score : MonoBehaviour
{

    public Text pirateScore, britishScore;
    public static int pirateScoreInt = 0;
    public static int britishScoreInt = 0;
    // Use this for initialization
    void Start()
    {
        pirateScore = GameObject.Find("pirateScore").GetComponent<Text>();
        britishScore = GameObject.Find("royalScore").GetComponent<Text>();
        Debug.Log(SceneManager.GetActiveScene().name.ToString());
    }

    // Update is called once per frame
    void Update()
    {
        //Updating score
        pirateScore.text = pirateScoreInt.ToString();
        britishScore.text = britishScoreInt.ToString();
        loadNext();

    }

    void OnCollisionEnter2D(Collision2D col)
    {

        // Pirates Score
        if (col.gameObject.name == "rightWall")
        {
            //Increase Pirate score by 1
            pirateScoreInt += checkLevel();
            //Respawn ball in center
            transform.position = new Vector3(0, 0, 0);
        }

        // British Score
        else if (col.gameObject.name == "leftWall")
        {
            //Increase British army score by 1
            britishScoreInt += checkLevel();
            //Respawn Ball in center
            transform.position = new Vector3(0, 0, 0);
        }
    }
    //Loads next level if score goal is reached or exceeded
    public void loadNext()
    {
        //The amount changes depending on the level. Level 2 gives 2 points for each score
        int scoreAmount;
        //getting current level
        string currentScene = SceneManager.GetActiveScene().name.ToString();
        
        if (currentScene == "Level1")
        {
            scoreAmount = 1;
            if (pirateScoreInt >= 2 || britishScoreInt >= 2)
            {
                Debug.Log("Level 2 loaded");
                Application.LoadLevel("Level2");
            }
        }

        else if (currentScene == "Level2")
        {
            scoreAmount = 2;
            if (pirateScoreInt >= 10 || britishScoreInt >= 10)
            {
                Application.LoadLevel("Level3");
            }
        }

        else if (currentScene == "Level3")
        {
            scoreAmount = 3;
            if (pirateScoreInt >= 20 || britishScoreInt >= 20)
            {
                Application.LoadLevel("EndScene");
            }
        }
    }

    //This method changes the amount of points rewarded to who ever scores depending on which level he scored
    public int checkLevel()
    {
        //Stores the name of the current Scene
        string currentScene = SceneManager.GetActiveScene().name.ToString();

        int scoreAmount = 0;
        if (currentScene == "Level1")
        {
            scoreAmount = 1;
        }
        else if (currentScene == "Level2")
        {
            scoreAmount = 2;
        }
        else if (currentScene == "Level3")
        {
            scoreAmount = 4;
        }
        return scoreAmount;
    }
    //This method is called to reset the score to 0 after game is finished and players choose to go back to the menu
    public void onMenuButton()
    {
        pirateScoreInt = 0;
        britishScoreInt = 0;
        Application.LoadLevel("MenuScene");
    }
}
